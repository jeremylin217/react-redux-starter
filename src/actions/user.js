import { createAction } from 'redux-actions'
import { CALL_API } from 'middleware/api'

export const unauthorized = createAction('UNAUTHORIZED')

export function getUser (id) {
  return {
    [CALL_API]: {
      types: [
        'GET_USER_REQUEST',
        'GET_USER_RECEIVE',
        'GET_USER_FAILURE'
      ],
      endpoint: `user/${id}`,
      method: 'GET',
      isAuth: true
    }
  }
}
